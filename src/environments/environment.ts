// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:
  {
    apiKey: "AIzaSyBdpPNr6WWU6_xEVCRg4pgboFUBBd4EHA4",
    authDomain: "cleancar-bbdfd.firebaseapp.com",
    projectId: "cleancar-bbdfd",
    storageBucket: "cleancar-bbdfd.appspot.com",
    messagingSenderId: "813470065442",
    appId: "1:813470065442:web:e9bf7f49d8478b49523b7b",
    measurementId: "G-WVS2J87TCZ",
    databaseURL: "https://cleancar-bbdfd-default-rtdb.europe-west1.firebasedatabase.app/"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
