import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { ServicesComponent } from './services/services.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { AdminServicesComponent } from './admin/admin-services/admin-services.component';
import { LoginComponent } from './login/login.component';
import { AuthorizationService } from './authorization.service';
import { AuthGuardService } from './userauthorization-guard.service';
import { UserService } from './user.service';
import { AdminAuthorizationGuardService } from './admin-authorization-guard.service';
import { ServiceFormComponent } from './admin/service-form/service-form.component';
import { CategoryService } from './category.service';
import { ServiceService } from './service.service';
import { ServiceFilterComponent } from './services/service-filter/service-filter.component';
import { ServicePageComponent } from './service-page/service-page.component';
import { ServiceCardComponent } from './service-card/service-card.component';
import { CheckoutService } from './checkout.service';
import { OrderServiceService } from './order-service.service';
import { RouterTestingModule } from '@angular/router/testing';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ServicesComponent,
    CheckoutComponent,
    OrderSuccessComponent,
    MyOrdersComponent,
    AdminServicesComponent,
    LoginComponent,
    ServiceFormComponent,
    ServiceFilterComponent,
    ServicePageComponent,
    ServiceCardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterTestingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    NgbModule,
    RouterModule.forRoot([
		{ path:'login'         , component:LoginComponent },
		{ path:''              , component:HomeComponent  },
    { path:'service-page'  , component:ServicesComponent },
        { path:'services'          , component:ServicesComponent },
        { path:'checkout'          , component:CheckoutComponent },

        { path:'order-success/:id' , component:OrderSuccessComponent , canActivate: [AuthGuardService] },
        { path:'my/orders'         , component:MyOrdersComponent     , canActivate: [AuthGuardService] },

        { path:'admin/services/new', component:ServiceFormComponent  , canActivate: [AuthGuardService, AdminAuthorizationGuardService] },
        { path:'admin/services/:id', component:ServiceFormComponent  , canActivate: [AuthGuardService, AdminAuthorizationGuardService] },
        { path:'admin/services'    , component:AdminServicesComponent, canActivate: [AuthGuardService, AdminAuthorizationGuardService] },
    ])
  ],
  providers: [
    AuthorizationService,
    AuthGuardService,
    AdminAuthorizationGuardService,
    UserService,
    CategoryService,
    ServiceService,
    CheckoutService,
    OrderServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
