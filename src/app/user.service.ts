import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import firebase from 'firebase';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { AppUser } from './models/app-user';

@Injectable({
  providedIn: 'root'
})

/**
 * UserService is responsible for the users in firebase
 */
export class UserService {

  appU : AppUser | undefined;
  users: any;
  other: any;

  constructor( private database: AngularFireDatabase ) { }

  /**
   * Saves a user to database
   */
  save( user: firebase.User )
  {
    let today   : Date   = new Date();
    let datetime: string = today.getFullYear() + "/" + today.getMonth() + "/" + today.getDay() + " " + today.getHours() + ":" + today.getMinutes();
    let userRole: string = "user";

    this.database.list("/users").valueChanges().subscribe(value => this.other = value );
    console.log(this.other);

    this.database.object( '/users/' + user.uid ).update({
      name : user.displayName,
      email: user.email,
      date : datetime
    });
  }

  /**
   * Returns with all the users stored in firebase
   */
  getAllUser() {
    return this.database.list("/users").valueChanges();
  }

  /**
   * Return with a user based on its Id
   */
  get(uid: string): Observable<AppUser | unknown> {
    let b = this.database.object<AppUser>('/users/' + uid).valueChanges();
    return b;
  }

}
