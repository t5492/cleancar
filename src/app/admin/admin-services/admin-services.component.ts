import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MyService } from 'src/app/models/service';
import { ServiceService } from 'src/app/service.service';

@Component({
  selector: 'app-admin-services',
  templateUrl: './admin-services.component.html',
  styleUrls: ['./admin-services.component.css']
})

/**
 * AdminServiceComponent list all the created Services.
 */
export class AdminServicesComponent implements OnInit, OnDestroy {

  services: Partial<MyService>[] = [];
  filteredServices: any[] = [];
  subscription: Subscription | undefined;

/**
 * Init services and filteredServices with ServiceService.
 */
  constructor( private serviceService: ServiceService ) {

    this.subscription = this.serviceService.getAllServices()
      .subscribe((actions: any[]) => {
        this.services = [];

        actions.forEach(action => {
          const val: any = action.payload.val();
          this.services.push({
            $key: action.key ? action.key : '', ...<Object>action.payload.val()
          });
        });

        this.filteredServices = this.services;
      });

  }

  ngOnInit(): void {
  }

/**
 * Unsubscribe from subscription.
 */
  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

}
