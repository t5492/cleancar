import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { CategoryService } from 'src/app/category.service';
import { ICategories } from 'src/app/models/categories.model';
import { ServiceService } from 'src/app/service.service';

@Component({
  selector: 'app-service-form',
  templateUrl: './service-form.component.html',
  styleUrls: ['./service-form.component.css']
})

/**
 * AdminServiceComponent list a selected Service.
 */
export class ServiceFormComponent implements OnInit {

  categories$: Observable<ICategories[]> | undefined;
  service: any = {};
  id: any;

  constructor( private categoryService: CategoryService, private myService_Service: ServiceService, private router: Router, private route: ActivatedRoute ) {
    this.categories$ = categoryService.getAll();

    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) this.myService_Service.getService(this.id).pipe(take(1)).subscribe((p: any) => this.service = p);
  }

/**
 * Saves a new or edited service with ServiceService.
 */
  save( service : any ) {
    if (this.id) this.myService_Service.update(this.id, service);
    else this.myService_Service.create( service );

    this.router.navigate(['/admin/services']);
  }

/**
 * Delete a choosen Service.
 */
  delete() {
    if (!confirm('Are you sure you want to delete this service?')) return;
    
    this.myService_Service.delete(this.id);
    this.router.navigate(['/admin/services']);
  }

  ngOnInit(): void {
  }

}
