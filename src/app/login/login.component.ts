import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../authorization.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
/**
 * LoginComponent shows the Google login page.
 */
export class LoginComponent{

  constructor( private auth: AuthorizationService ) { }

/**
 * With login button, route the user to Google login interface.
 */
  login()
  {
		this.auth.login();
  }

}
