import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthorizationService } from './authorization.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

/**
 * AuthGuardService is responsible for the users functions they can use
 */
export class AuthGuardService implements CanActivate{

  constructor( private auth:AuthorizationService, private router:Router ) { }

  /**
   * If a user logged in returns true, else returns false
   */
  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {
    return this.auth.user$.pipe(map((user: any) => {
        if( user != null ) {
          return true;
        }
        else {
          this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });

          return false;
        }
      }));
  }
}
