import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ActivatedRoute } from '@angular/router';
import firebase from 'firebase';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AppUser } from './models/app-user';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})

/**
 * AuthorizationService is responsible for the authorization functions
 */
export class AuthorizationService {

  /**
   * Firebase user
   */
  user$!: Observable<firebase.User | null>;

  constructor( private afAuth:AngularFireAuth, private route: ActivatedRoute, private userService: UserService, ) {
    this.user$ = afAuth.authState;
  }

  /**
   * Routes to the Google authentication interface
   */
  login()
  {
    let returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
    localStorage.setItem( 'returnUrl', returnUrl );
    this.afAuth.signInWithRedirect( new firebase.auth.GoogleAuthProvider() );
  }

  /**
   * Logs out the user
   */
  logout()
  {
    this.afAuth.signOut();
  }

  /**
   * Returns with the loggen in user
   */
  get appUser$(): Observable<AppUser | unknown> {
    return this.user$
      .pipe(
        switchMap((user: any) => {
          if (user) {
            return this.userService.get(user.uid);
          }

          return of(null);
        })
      );
  }
}