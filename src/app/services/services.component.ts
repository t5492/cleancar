import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { CategoryService } from '../category.service';
import { MyService } from '../models/service';
import { Checkout } from '../models/checkout';
import { ServiceService } from '../service.service';
import { CheckoutService } from '../checkout.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})

/**
 * ServicesComponent lists all the avilable services
 */
export class ServicesComponent implements OnInit {

  /**
   * Stores the avilable services
   */
  services: MyService[] = [];
  /**
   * Stores the filtered services
   */
  filteredServices: MyService[] = [];
  /**
   * Stores the selected category
   */
  category: string | undefined;
  /**
   * Checkout Observable
   */
  checkoutObs$: Observable<Checkout> | undefined;

  constructor( private serviceService: ServiceService, private route: ActivatedRoute, private router: Router, private categoryService: CategoryService, private checkoutService: CheckoutService ) { }

  async ngOnInit() {
    this.checkoutObs$ = await this.checkoutService.getCheckout();
    this.populateServices();
  }

  /**
   * Routes to Checkout page
   */
  goToCheckout() {
    this.router.navigate(['/checkout']);
  }

  /**
   * Shows the services based on the selected category
   */
  private populateServices() { 
    this.serviceService
      .getAllServices()
      .pipe(
        switchMap((actions: any[]) => {
          this.services = [];
  
          actions.forEach(action => {
            const val: any = action.payload.val();
  
            this.services.push({
              $key: <string>action.key, 
              title: <string>val.title,
              price: <number>val.price, 
              category: <string>val.category,
              imageUrl: <string>val.imageUrl
            });
          });

          return this.route.queryParamMap;
        }
        )
      )
      .subscribe((params: any) => {
        this.category = params.get('category');
        this.applyFilter();
      });
  }

  /**
   * Applies the selected category
   */
  private applyFilter() { 
    this.filteredServices = (this.category) 
      ? this.services.filter(s => s.category === this.category) 
      : this.services;
  }

}
