import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoryService } from 'src/app/category.service';
import { ICategories } from 'src/app/models/categories.model';
import { ServiceService } from 'src/app/service.service';

@Component({
  selector: 'app-service-filter',
  templateUrl: './service-filter.component.html',
  styleUrls: ['./service-filter.component.css']
})

/**
 * ServiceFilterComponent filters the services based on the selected category
 */
export class ServiceFilterComponent {

  categories$: Observable<ICategories[]> | undefined;
  @Input('category') category!: any;

  constructor( categoryService: CategoryService ) {
    this.categories$ = categoryService.getAll();
   }
}
