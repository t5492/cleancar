import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { ICategories } from './models/categories.model';

@Injectable({
  providedIn: 'root'
})

/**
 * AuthorizationService is responsible for getting the categories from firebase database
 */
export class CategoryService {

  constructor( private db: AngularFireDatabase ){ }

  /**
   * Returns with all the categories stored in the database
   */
  getAll(): Observable<ICategories[]> | undefined {
    const data: any = this.db.list('/categories', ref => ref.orderByChild('name')).valueChanges();
    
    return data || undefined;
  }
}
