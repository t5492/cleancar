import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AuthorizationService } from '../authorization.service';
import { UserOrder } from '../models/order';
import { Checkout } from '../models/checkout';
import { CheckoutService } from '../checkout.service';
import { OrderServiceService } from '../order-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})

/**
 * CheckoutComponent list all the items added to the checkout list.
 */
export class CheckoutComponent implements OnInit, OnDestroy {

  orderInfo = {
    name: '',
    addressLine1: '',
    addressLine2: '',
    city: ''
  };

  userSubscription!: Subscription;
  userId           : string = '';
  checkout!        : Checkout;
  checkoutObs$!    : Observable<Checkout>;

  constructor( private checkoutService: CheckoutService, private authorizationService: AuthorizationService, private orderService: OrderServiceService, private router: Router ) { }

  async ngOnInit() {
    this.checkoutObs$ = await this.checkoutService.getCheckout();

    (await this.checkoutService.getCheckout()).subscribe( checkout => this.checkout = checkout );

    this.userSubscription = this.authorizationService.user$.subscribe(
      (user: any) => this.userId = user.uid);
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

/**
 * Create a new UserOder based on the filled informations nad navigates to ored-success page.
 */
  async placeNewUserOrder() {

    console.log(this.checkout.totalPrice);
    console.log(this.userId);
    console.log(this.orderInfo);

    let userOrder  = new UserOrder(this.userId, this.orderInfo, this.checkout);
    let result = await this.orderService.placeNewUserOrder(userOrder);
    this.router.navigate(['/order-success', result.key]);
  }


/**
 * Clear all the items form Checkout list.
 */
  clearCheckout() {
    this.checkoutService.clearCheckout();
  }
}
