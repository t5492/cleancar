import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { take } from 'rxjs/operators';
import { AuthorizationService } from '../authorization.service';
import { AppUser } from '../models/app-user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

/**
 * HomeComponent shows the main page.
 */
export class HomeComponent implements OnInit {
  appUser!: AppUser;
  users: any;

  constructor(private auth: AuthorizationService, private db: AngularFireDatabase, private userService: UserService) {
    auth.appUser$.subscribe( (appUser : any ) => this.appUser = appUser);
    userService.getAllUser().pipe(take(1)).subscribe((users: any) => this.users = users);
  }

  ngOnInit(): void {
  }

/**
 * Shows the user count based on how many user ordered in the application.
 */
  getAllUserCount(){
    let userCount: number = 0;

    if( this.users != null )
      userCount = this.users.length

    return userCount;
  }

  testMethod() {
    return 1;
  }
}
