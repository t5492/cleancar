import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthorizationService } from '../authorization.service';
import { AppUser } from '../models/app-user';
import { Checkout } from '../models/checkout';
import { CheckoutService } from '../checkout.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

/**
 * NavbarComponent shows the navigation bar on every page at the top
 */
export class NavbarComponent implements OnInit{
	checkoutObs$: Observable<Checkout> | undefined;
	user : AppUser | undefined;
  isUser : boolean = false;

  constructor( private auth: AuthorizationService, private checkoutService: CheckoutService )
  {  }

  /**
   * Logs out the user
   */
  logout()
  {
	  this.auth.logout();
    this.isUser = false;
  }

  async ngOnInit(): Promise<void> {
    this.auth.appUser$.subscribe((user: any) => {
      this.user = user;
      this.isUser = true;
    });

    this.checkoutObs$ = await this.checkoutService.getCheckout();
  }
}
