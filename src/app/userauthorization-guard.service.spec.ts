import { TestBed } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from 'src/environments/environment';

import { AuthGuardService } from './userauthorization-guard.service';

describe('UserAuthorizationGuardService', () => {
  let service: AuthGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AngularFireModule.initializeApp(environment.firebase),
      ],
    });
    service = TestBed.inject(AuthGuardService);
  });

  it('test_01_should_create_component_succesfully', () => {
    expect(service).toBeTruthy();
  });
});
