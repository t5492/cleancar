import { MyService } from './service';

export class CheckoutItem {
	/**
   * Title of an Item
   */
  title   !: string;
	/**
   * Key/Id of an Item
   */
  $key    !: string;
	/**
   * Price of an Item
   */
  price   !: number;
	/**
   * Category of an Item
   */
  category!: string;
	/**
   * Quantity of an Item
   */
  quantity!: number;

  constructor(init?: Partial<CheckoutItem>) {
    Object.assign(this, init);
  }

}

export class Checkout {

	/**
   * List of CheckoutItems
   */
  items: CheckoutItem[] = [];

  constructor(
    private map: { [id: string]: CheckoutItem }
    ) {

    this.map = map || {};

    for (let id in map) {
      let item = map[id];
      this.items.push(
        new CheckoutItem({ ...item, $key: id }
      ));
    }
  }
	/**
   * Returns with the total count of Items
   */
  get totalItemsCount() {
    let count = 0;
    for (let i in this.map) {
      count += 1;
    }
    return count;
  }

  /**
   * Returns with the total price of Items
   */
  get totalPrice() {
    let sum = 0;
    for (let id in this.items)
      sum += this.items[id].price;
    return sum;
  }

  /**
   * Returns with the quantity of and Item
   */
  getQuantity( service: MyService ) {
    let item = this.map[service.$key];
    return item ? item.quantity : 0;
  }
}