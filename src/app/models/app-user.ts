export interface AppUser
{
	/**
     * Name of a user
     */
	name: string,
	/**
     * Email of a user
     */
	email: string,
	/**
     * Admin role of a user
     */
	isAdmin: boolean,
	/**
     * Role of an user
     */
	userRole : string
}