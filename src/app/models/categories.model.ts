export interface ICategories {
	/**
     * Name of category
     */
    name: string;
}