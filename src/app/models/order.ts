import { Checkout } from './checkout';

export class UserOrder {

  /**
   * When the order was placed
   */
  datePlaced: number;

  /**
   * All the CheckoutItems
   */
  items: any[];

  constructor(public userId: string, public orderInfo: any, checkoutList: Checkout) {
    this.datePlaced = new Date().getTime();

    this.items = checkoutList.items.map(i => {
      return {
        title: i.title,
        price: i.price,
        quantity: i.quantity,
        totalPrice: i.price,
		    productId: i.$key
      }
    })
  }
}