export interface MyService {
    /**
     * Key/Id of a Service
     */
    $key: string;
    /**
     * Title of a Service
     */
    title: string;
    /**
     * Price of a Service
     */
    price: number;
    /**
     * Category of a Service
     */
    category: string;
    /**
     * Image URL of a Service
     */
    imageUrl: string;
  }