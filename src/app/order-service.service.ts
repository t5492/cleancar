import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { CheckoutService } from './checkout.service';

@Injectable({
  providedIn: 'root'
})

/**
 * CheckoutService is responsible for getting the orders from firebase
 */
export class OrderServiceService {

  constructor( private db: AngularFireDatabase, private checkoutService: CheckoutService )
  { }

  /**
   * Returns with all the orders based on the user Id
   */
  getOrdersByUser(id: string) {
    return this.db.list(
      '/orders', ref => ref.orderByChild('userId').equalTo(id)
      )
      .valueChanges();
  }

  /**
   * Creates a new order in firebase
   */
  async placeNewUserOrder(order: unknown) {
    let result = await this.db.list('/orders').push(order);
    this.checkoutService.clearCheckout();
    return result;
  }
}
