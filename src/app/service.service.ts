import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})

/**
 * ServiceService is responsible for getting the services from/to firebase
 */
export class ServiceService {

  constructor( private db: AngularFireDatabase ) { }

  /**
   * Creates a new service in firebse
   */
  create( service: any ) {
    return this.db.list("/services").push( service );
  }

  /**
   * Updates a service in firebase
   */
  update(serviceId: string, service: Partial<unknown>) {
    return this.db.object('/services/' + serviceId).update(service);
  }

  /**
   * Deletes a service in firebase
   */
  delete(serviceId: string | null) {
    return this.db.object('/services/' + serviceId).remove();
  }

  /**
   * Returns with all the services stored in firebase
   */
  getAllServices() {
    return this.db.list("/services").snapshotChanges();
  }

  /**
   * Returns with a service by its Id
   */
  getService(serviceId: string) {
    return this.db.object('/services/' + serviceId).valueChanges();
  }
}
