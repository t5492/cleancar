import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorizationService } from './authorization.service';
import { map } from 'rxjs/operators';
import { tap } from 'rxjs/operators';
import { switchMap } from 'rxjs/operators';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})

/**
 * AdminAuthorizationGuardService gets the rule of a user
 */
export class AdminAuthorizationGuardService implements CanActivate {

  constructor( private auth: AuthorizationService, private userService: UserService ) { }

  /**
   * If the user has Admin rule return true, else false
   */
  canActivate(): Observable<boolean> { 
    return this.auth.appUser$
      .pipe(
        map((appUser: any) => appUser.userRole == "admin")
      );
  }
}
