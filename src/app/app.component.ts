import { Component } from '@angular/core';
import { Router, UrlTree } from '@angular/router';
import { AuthorizationService } from './authorization.service';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

/**
 * AppComponent is the base component of the application
 */
export class AppComponent {
  constructor(private userService: UserService, private auth: AuthorizationService, router: Router )
  {
    auth.user$.subscribe( user => {
      if( user != null ) {
        this.userService.save( user );

        let returnUrl = localStorage.getItem( 'returnUrl' );

        if( returnUrl != null ) {
          localStorage.removeItem('returnUrl');
          router.navigateByUrl( returnUrl );
        }
      }
    });
  }

  testMethod() {
    return 1;
  }
}
