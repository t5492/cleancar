import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { MyService } from './models/service';
import { Checkout } from './models/checkout';

@Injectable({
  providedIn: 'root'
})

/**
 * CheckoutService is responsible for the CheckoutList
 */
export class CheckoutService {

  constructor(private db: AngularFireDatabase) { }

  /**
   * Adds a Service to CheckoutList
   */
  async addToCheckout( service: MyService ) {
    this.updateItem(service, 1);
  }

  /**
   * Removes a Service to CheckoutList
   */
  async removeFromCheckout( service: MyService ) {
    this.updateItem(service, 0);
  }

  /**
   * Creates a Checkout
   */
  private create() {
    console.log("create");
    let date = new Date().getTime();

    return this.db.list('/checkouts').push({
      dateCreated: date
    });
  }

  /**
   * If a Checkout exists returns with the Checkout, if not, creaates a new one
   */
  private async getOrCreateCheckoutId(): Promise<string> {
    let checkoutId = localStorage.getItem('checkoutId');
    console.log("checkout id:" + checkoutId);

    if (checkoutId) return checkoutId;

    let result = await this.create();
    localStorage.setItem('checkoutId', result.key || '');

    return result.key || '';
  }

  /**
   * Returns a Service from the CheckoutList based on its Id
   */
  private getItem(checkoutId: string, serviceId: string) {
    return this.db.object('/checkouts/' + checkoutId + '/items/' + serviceId);
  }

  /**
   * Updates a Sevice in the CheckoutList
   */
  private async updateItem(service: MyService, change: number) {
    let checkoutId = await this.getOrCreateCheckoutId();
    let item$ = this.getItem(checkoutId, service.$key);
    item$.valueChanges().pipe(take(1)).subscribe((item: any) => {

      let quantity = change;
      if (quantity === 0) item$.remove();
      else item$.update({ 
        title: service.title,
        imageUrl: service.imageUrl,
        price: service.price,
        quantity: quantity
      });
    });
  }

  /**
   * Clears the items in CheckoutList
   */
  async clearCheckout() {
    let checkoutId = await this.getOrCreateCheckoutId();
    this.db.object('/checkouts/' + checkoutId + '/items').remove();
  }

  /**
   * Returns with teh Checkout
   */
  async getCheckout(): Promise<Observable<Checkout>> {
    console.log("getCheckout method");

    let checkoutId = await this.getOrCreateCheckoutId();
    return this.db.object('/checkouts/' + checkoutId)
      .valueChanges()
      .pipe(map((x: any) => new Checkout(x.items)));
  }
}
