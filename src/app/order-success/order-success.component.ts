import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order-success',
  templateUrl: './order-success.component.html',
  styleUrls: ['./order-success.component.css']
})

/**
 * NavbarComponent shows a secces page if the order was succesgful
 */
export class OrderSuccessComponent implements OnInit {

  constructor(private router: Router) {}

  ngOnInit(): void {
  }

  /**
   * Routes to my/orders
   */
  routeToMyOrders(){
    this.router.navigate(['/my/orders']);
  }

}
