import { Component, Input, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { MyService } from '../models/service';
import { Checkout } from '../models/checkout';
import { CheckoutService } from '../checkout.service';

@Component({
  selector: 'app-service-card',
  templateUrl: './service-card.component.html',
  styleUrls: ['./service-card.component.css']
})

/**
 * ServiceCardComponent is a card module. Can be integrated to many places
 */
export class ServiceCardComponent {

  @Input('service') service!: MyService;
  @Input('checkout') checkout!: Checkout;

  constructor( private checkoutService: CheckoutService, private db: AngularFireDatabase ) { }

  /**
   * Adds a Service to the CheckouList
   */
  addToCheckout() {
    this.checkoutService.addToCheckout( this.service );
  }

  /**
   * Removes a Service to the CheckouList
   */
  removeFromCheckout() {
    this.checkoutService.removeFromCheckout( this.service );
  }

  /**
   * Returns with the count of all the services in the CheckoutList
   */
  getServiceQuantity() {
    if( this.checkout == null )
      return 0;

    return this.checkout.getQuantity( this.service );
  }
}
