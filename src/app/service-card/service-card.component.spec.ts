import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from 'src/environments/environment';
import { MyService } from '../models/service';

import { ServiceCardComponent } from './service-card.component';

describe('ServiceCardComponent', () => {
  let component: ServiceCardComponent;
  let fixture: ComponentFixture<ServiceCardComponent>;

  const mockService: MyService = {
    $key: "test",
    title: "test",
    price: 5,
    category: "test",
    imageUrl: "https://firebasestorage.googleapis.com/v0/b/cleancar-bbdfd.appspot.com/o/service_captures%2Fbell_washing.jpg?alt=media&token=63db929a-46a9-4579-899d-5a14698e7800"
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AngularFireModule.initializeApp(environment.firebase),
      ],
      declarations: [ ServiceCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceCardComponent);
    component = fixture.componentInstance;
    component.service = mockService;
    fixture.detectChanges();
  });

  it('test_01_should_create_component_succesfully', () => {
    expect(component).toBeTruthy();
  });

  it('test_02_service_quantity_if_null', () => {
    expect(component.getServiceQuantity()).toEqual(0);
  });
});
