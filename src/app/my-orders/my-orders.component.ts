import { Component, OnInit } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { AuthorizationService } from '../authorization.service';
import { OrderServiceService } from '../order-service.service';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})

/**
 * MyOrdersComponent list all previous orders
 */
export class MyOrdersComponent implements OnInit {

  /**
   * Returns with the total price of an order
   */
  getTotalPriceOfOrder(order: any){
    var price: number = 0;

    for( var orderitem of order.items ){
      price += orderitem.price
    }

    return price + " HUF"
  }

  myOrders$: any;
  constructor(private orderService: OrderServiceService, private authorizationService: AuthorizationService) {
    this.myOrders$ = authorizationService.user$.pipe(switchMap((u: any) => orderService.getOrdersByUser(u.uid)));
  }

  ngOnInit(): void {
  }

}
