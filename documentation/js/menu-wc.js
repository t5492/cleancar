'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">clean-car documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-b5759ed58f5ed0e7d8dfda5ee3b30e391862412e545bdc87a97a3efd700c19623349608479430e93107e0dceb91e5d3fdf5b9b75b7f5a434d4d0e83a59172cd5"' : 'data-target="#xs-components-links-module-AppModule-b5759ed58f5ed0e7d8dfda5ee3b30e391862412e545bdc87a97a3efd700c19623349608479430e93107e0dceb91e5d3fdf5b9b75b7f5a434d4d0e83a59172cd5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-b5759ed58f5ed0e7d8dfda5ee3b30e391862412e545bdc87a97a3efd700c19623349608479430e93107e0dceb91e5d3fdf5b9b75b7f5a434d4d0e83a59172cd5"' :
                                            'id="xs-components-links-module-AppModule-b5759ed58f5ed0e7d8dfda5ee3b30e391862412e545bdc87a97a3efd700c19623349608479430e93107e0dceb91e5d3fdf5b9b75b7f5a434d4d0e83a59172cd5"' }>
                                            <li class="link">
                                                <a href="components/AdminServicesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AdminServicesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CheckoutComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CheckoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HomeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MyOrdersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MyOrdersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NavbarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/OrderSuccessComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >OrderSuccessComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServiceCardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServiceCardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServiceFilterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServiceFilterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServiceFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServiceFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServicePageComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServicePageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ServicesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServicesComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-b5759ed58f5ed0e7d8dfda5ee3b30e391862412e545bdc87a97a3efd700c19623349608479430e93107e0dceb91e5d3fdf5b9b75b7f5a434d4d0e83a59172cd5"' : 'data-target="#xs-injectables-links-module-AppModule-b5759ed58f5ed0e7d8dfda5ee3b30e391862412e545bdc87a97a3efd700c19623349608479430e93107e0dceb91e5d3fdf5b9b75b7f5a434d4d0e83a59172cd5"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-b5759ed58f5ed0e7d8dfda5ee3b30e391862412e545bdc87a97a3efd700c19623349608479430e93107e0dceb91e5d3fdf5b9b75b7f5a434d4d0e83a59172cd5"' :
                                        'id="xs-injectables-links-module-AppModule-b5759ed58f5ed0e7d8dfda5ee3b30e391862412e545bdc87a97a3efd700c19623349608479430e93107e0dceb91e5d3fdf5b9b75b7f5a434d4d0e83a59172cd5"' }>
                                        <li class="link">
                                            <a href="injectables/AuthorizationService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AuthorizationService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/CategoryService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CategoryService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/CheckoutService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CheckoutService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/OrderServiceService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >OrderServiceService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ServiceService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServiceService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Checkout.html" data-type="entity-link" >Checkout</a>
                            </li>
                            <li class="link">
                                <a href="classes/CheckoutItem.html" data-type="entity-link" >CheckoutItem</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserOrder.html" data-type="entity-link" >UserOrder</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AuthorizationService.html" data-type="entity-link" >AuthorizationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CategoryService.html" data-type="entity-link" >CategoryService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CheckoutService.html" data-type="entity-link" >CheckoutService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/OrderServiceService.html" data-type="entity-link" >OrderServiceService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ServiceService.html" data-type="entity-link" >ServiceService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link" >UserService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AdminAuthorizationGuardService.html" data-type="entity-link" >AdminAuthorizationGuardService</a>
                            </li>
                            <li class="link">
                                <a href="guards/AuthGuardService.html" data-type="entity-link" >AuthGuardService</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/AppUser.html" data-type="entity-link" >AppUser</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ICategories.html" data-type="entity-link" >ICategories</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MyService.html" data-type="entity-link" >MyService</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});